Rails.application.routes.draw do
  get 'planner/index'
  get 'home/index'
    root 'home#index'
    delete 'home/index', to: 'courses_taken#destroy'
    resources :courses_taken
    resources :users
    resources :sessions, only: [:new, :create, :destroy]    
    get 'signup', to: 'users#new', as: 'signup'
    get 'login', to: 'sessions#new', as: 'login'
    get 'logout', to: 'sessions#destroy', as: 'logout'
end
