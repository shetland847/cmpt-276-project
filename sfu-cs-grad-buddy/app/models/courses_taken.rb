class CoursesTaken < ApplicationRecord
    validates :courseinstanceid, uniqueness: { scope: :userid, message: "only one courseinstane per user" }
    
    
    def self.completedCourses(userid)
        currentsemester = currentSemester
        if(userid != nil)
            
            coursespersemester1 = Hash.new()

            semesters = CourseInstance.allSemesters
            
            userid = userid.to_s
            
            semesters.each do |semester|
            
                sql = "SELECT courses.coursename, courses.department, courses.coursenumber, course_instances.instructor, courses_takens.grade, courses.credits, courses_takens.status, courses_takens.grade
                        FROM courses_takens 
                            LEFT JOIN course_instances
                            ON courses_takens.courseinstanceid = course_instances.courseinstanceid
                            LEFT JOIN courses
                            ON course_instances.courseid = courses.courseid 
                        WHERE courses_takens.userid = '" + userid  + "' AND course_instances.semester = '" + (semester.semester).to_s + "'"
                
                records = ActiveRecord::Base.connection.execute(sql)
            
                coursespersemester1[dateToSemesterString(semester.semester)] = records
            end
            
            return coursespersemester1
            
        end
        
        
        
        
    end
    
end
