class CourseInstance < ApplicationRecord
    def self.futureSemesters
        CourseInstance.where("semester > ?", ApplicationRecord.currentSemester).select(:semester).distinct.order(:semester)
        
    end
    
    
     def self.allSemesters
        CourseInstance.select(:semester).distinct.order(:semester)
        
    end
    
    def self.coursesInFutureSemesters(userid)
        
        coursespersemester = Hash.new()
            
        futuresemesters = futureSemesters
        
        futuresemesters.each do |semester|
            
                            
            if(userid)
                sql = "SELECT courses.coursename, courses.department, courses.coursenumber,  course_instances.instructor, courses.credits, foo.status, course_instances.courseinstanceid
                        FROM course_instances 
                            LEFT JOIN courses ON course_instances.courseid = courses.courseid   
                            LEFT JOIN (SELECT * FROM courses_takens WHERE courses_takens.userid = '" + userid + "') AS foo
                            ON foo.courseinstanceid = course_instances.courseinstanceid 
                        WHERE course_instances.semester = '" + (semester.semester).to_s + "'"
                
                records = ActiveRecord::Base.connection.execute(sql)
                
                coursespersemester[dateToSemesterString(semester.semester)] = records
                
            else
                courses = CourseInstance.select('*').where("semester = ?", semester.semester).joins("LEFT JOIN courses ON course_instances.courseid = courses.courseid")

                coursespersemester[dateToSemesterString(semester.semester)] = courses
                
            end

        end
            
        return coursespersemester    
    end
        
        
end
