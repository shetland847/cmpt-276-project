class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
  
  def self.currentSemester
    Date.parse('2019-05-01') #month 01 => spring, month 05 => summer, month 09 => fall
  end
  
  def self.dateToSemesterString(date)
      
      @hash = {"01" => "Spring", "05" => "Summer", "09" => "Fall"}     
      
      datestring = date.to_s
      
      semester = @hash[datestring[5..6]]
      year = datestring[0..3]

      return semester + ' ' + year  
      
  end
  
end
