class HomeController < ApplicationController
  def index
      @courses = Course.all
      
      @futureSemesters = CourseInstance.futureSemesters
      
      if(current_user)
        @coursesInFutureSemesters = CourseInstance.coursesInFutureSemesters(current_user.userid)
      else
        @coursesInFutureSemesters = CourseInstance.coursesInFutureSemesters(current_user)
      end
      
      
      
      
      
      if(current_user)
        @blah = CoursesTaken.completedCourses(current_user.userid)
      end
      
      
      @currentSemester = ApplicationRecord.dateToSemesterString(ApplicationRecord.currentSemester)
      
  end
end
