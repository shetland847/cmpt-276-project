class CoursesTakenController < ApplicationController
    def create
        if(current_user)
            cid = params[:courseinstanceid]
            blah = CoursesTaken.create(courseinstanceid: cid, userid: current_user.userid, status: 'planned')
            if(blah.valid?)
                redirect_to root_url

            else
                redirect_to root_url


            end
        end
        
    end
    
    def destroy
        if(current_user)
            cid = params[:courseinstanceid]
            course = CoursesTaken.where("courseinstanceid = ? AND userid = ?", cid, current_user.userid)
            course.destroy(course[0][:id])
            
            redirect_to root_url

            
        end

    
    end
    
    
end
