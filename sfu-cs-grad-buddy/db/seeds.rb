# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)



Course.create(courseid: 1, coursename: 'Intro to Computing Science and Programming I', department: 'CMPT', coursenumber: 120, credits: 3.0)
Course.create(courseid: 2, coursename: 'Intro to Computing Science and Programming II', department: 'CMPT', coursenumber: 125, credits: 3.0)
Course.create(courseid: 3, coursename: 'Computing Laboratory', department: 'CMPT', coursenumber: 127, credits: 4.0)
Course.create(courseid: 4, coursename: 'Data Structures and Programming', department: 'CMPT', coursenumber: 225, credits: 3.0)
Course.create(courseid: 5, coursename: 'Intro to Software Engineering', department: 'CMPT', coursenumber: 276, credits: 3.0)
Course.create(courseid: 6, coursename: 'Operating Systems I', department: 'CMPT', coursenumber: 300, credits: 3.0)
Course.create(courseid: 7, coursename: 'Data Structures and Algorithms', department: 'CMPT', coursenumber: 307, credits: 3.0)
Course.create(courseid: 8, coursename: 'Intro to Computer Systems', department: 'CMPT', coursenumber: 295, credits: 3.0)
Course.create(courseid: 9, coursename: 'Discrete Math I', department: 'MACM', coursenumber: 101, credits: 3.0)
Course.create(courseid: 10, coursename: 'Discrete Math II', department: 'MACM', coursenumber: 201, credits: 3.0)


#semester shall be identified by month: fall = 09, spring = 01, summer = 05





CourseInstance.create(courseinstanceid: 1, courseid: 1, semester: '2019-09-01', instructor: 'Diana Culkierman')
CourseInstance.create(courseinstanceid: 2, courseid: 1, semester: '2020-01-01', instructor: 'Arrvindh Shriraman')
CourseInstance.create(courseinstanceid: 3, courseid: 2, semester: '2020-05-01', instructor: 'Brad Bart')
CourseInstance.create(courseinstanceid: 4, courseid: 2, semester: '2019-09-01', instructor: 'Steven Pearce')
CourseInstance.create(courseinstanceid: 5, courseid: 3, semester: '2020-01-01', instructor: 'Toby Donaldson')
CourseInstance.create(courseinstanceid: 6, courseid: 3, semester: '2020-05-01', instructor: 'Brian Fraser')
CourseInstance.create(courseinstanceid: 7, courseid: 4, semester: '2019-09-01', instructor: 'Anne Lavergne')
CourseInstance.create(courseinstanceid: 8, courseid: 4, semester: '2020-01-01', instructor: 'Jian Pei')
CourseInstance.create(courseinstanceid: 9, courseid: 5, semester: '2020-05-01', instructor: 'Eugenia Ternovska')
CourseInstance.create(courseinstanceid: 10, courseid: 5, semester: '2019-09-01', instructor: 'Kay C. Wiese')
CourseInstance.create(courseinstanceid: 11, courseid: 6, semester: '2020-01-01', instructor: 'KangKang Yin')
CourseInstance.create(courseinstanceid: 12, courseid: 6, semester: '2020-05-01', instructor: 'Greg Baker')
CourseInstance.create(courseinstanceid: 13, courseid: 7, semester: '2019-09-01', instructor: 'Andrei Bulatov')
CourseInstance.create(courseinstanceid: 14, courseid: 7, semester: '2020-01-01', instructor: 'Bobby Chan')
CourseInstance.create(courseinstanceid: 15, courseid: 8, semester: '2020-05-01', instructor: 'Steven Pearce')
CourseInstance.create(courseinstanceid: 16, courseid: 8, semester: '2019-09-01', instructor: 'Brad Bart')
CourseInstance.create(courseinstanceid: 17, courseid: 9, semester: '2020-01-01', instructor: 'Robert D. Cameron')
CourseInstance.create(courseinstanceid: 18, courseid: 9, semester: '2020-05-01', instructor: 'Ouldooz Baghban Karimi')
CourseInstance.create(courseinstanceid: 19, courseid: 10, semester: '2019-09-01', instructor: 'Valentine Kabanets')
CourseInstance.create(courseinstanceid: 20, courseid: 10, semester: '2019-09-01', instructor: 'Ramesh Krishnamurti')
CourseInstance.create(courseinstanceid: 21, courseid: 1, semester: '2019-05-01', instructor: 'Diana Culkierman')
CourseInstance.create(courseinstanceid: 22, courseid: 1, semester: '2018-09-01', instructor: 'Arrvindh Shriraman')
CourseInstance.create(courseinstanceid: 23, courseid: 2, semester: '2018-09-01', instructor: 'Brad Bart')
CourseInstance.create(courseinstanceid: 24, courseid: 2, semester: '2018-09-01', instructor: 'Steven Pearce')
CourseInstance.create(courseinstanceid: 25, courseid: 3, semester: '2019-01-01', instructor: 'Toby Donaldson')
CourseInstance.create(courseinstanceid: 26, courseid: 3, semester: '2019-01-01', instructor: 'Brian Fraser')
CourseInstance.create(courseinstanceid: 27, courseid: 4, semester: '2019-05-01', instructor: 'Anne Lavergne')
CourseInstance.create(courseinstanceid: 28, courseid: 4, semester: '2018-09-01', instructor: 'Jian Pei')
CourseInstance.create(courseinstanceid: 29, courseid: 5, semester: '2018-05-01', instructor: 'Eugenia Ternovska')                       
                       

                       
User.create(userid: 'rlintott', password: '12345', email: 'student@sfu.ca')
User.create(userid: 'shetland', password: '12345', email: 'student@sfu.ca')
User.create(userid: 'dliu', password: '12345', email: 'student@sfu.ca')


CoursesTaken.create(userid: 'rlintott', courseinstanceid: 21, grade: 'B', status: 'taken')
CoursesTaken.create(userid: 'rlintott', courseinstanceid: 22, grade: 'C', status: 'taken')
CoursesTaken.create(userid: 'rlintott', courseinstanceid: 23, grade: 'A', status: 'taken')
CoursesTaken.create(userid: 'rlintott', courseinstanceid: 24, grade: 'A-', status: 'taken')


CoursesTaken.create(userid: 'shetland', courseinstanceid: 25, grade: 'A+', status: 'taken')
CoursesTaken.create(userid: 'shetland', courseinstanceid: 26, grade: 'B+', status: 'taken')
CoursesTaken.create(userid: 'shetland', courseinstanceid: 21, grade: 'C', status: 'taken')
CoursesTaken.create(userid: 'shetland', courseinstanceid: 23, grade: 'C+', status: 'taken')


CoursesTaken.create(userid: 'dliu', courseinstanceid: 27, grade: 'A', status: 'taken')
CoursesTaken.create(userid: 'dliu', courseinstanceid: 28, grade: 'A', status: 'taken')
CoursesTaken.create(userid: 'dliu', courseinstanceid: 29, grade: 'B', status: 'taken')
CoursesTaken.create(userid: 'dliu', courseinstanceid: 21, grade: 'C-', status: 'taken')


CoursesTaken.create(userid: 'rlintott', courseinstanceid: 1, status: 'planned', grade: '')
CoursesTaken.create(userid: 'rlintott', courseinstanceid: 2, status: 'planned', grade: '')
CoursesTaken.create(userid: 'rlintott', courseinstanceid: 4, status: 'planned', grade: '')
CoursesTaken.create(userid: 'rlintott', courseinstanceid: 10, status: 'planned', grade: '')


CoursesTaken.create(userid: 'shetland', courseinstanceid: 12, status: 'planned', grade: '')
CoursesTaken.create(userid: 'shetland', courseinstanceid: 10, status: 'planned', grade: '')
CoursesTaken.create(userid: 'shetland', courseinstanceid: 13, status: 'planned', grade: '')
CoursesTaken.create(userid: 'shetland', courseinstanceid: 19, status: 'planned', grade: '' )


CoursesTaken.create(userid: 'dliu', courseinstanceid: 20, status: 'planned', grade: '' )
CoursesTaken.create(userid: 'dliu', courseinstanceid: 18, status: 'planned', grade: '' )
CoursesTaken.create(userid: 'dliu', courseinstanceid: 17, status: 'planned', grade: '')
CoursesTaken.create(userid: 'dliu', courseinstanceid: 2, status: 'planned', grade: '' )


