class CreateCourses < ActiveRecord::Migration[5.2]
  def change
    create_table :courses do |t|
        t.integer :courseid
        t.string :name
        t.string :semester
        t.string :instructor
      t.timestamps
    end
  end
end
