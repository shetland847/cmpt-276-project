class AddCreditsToCourses < ActiveRecord::Migration[5.2]
  def change
    add_column :courses, :credits, :integer
  end
end
