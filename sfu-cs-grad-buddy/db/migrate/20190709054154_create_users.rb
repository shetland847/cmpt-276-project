class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :userid
      t.string :password_digest

      t.timestamps
    end
    add_index :users, :userid, unique: true
  end
end
