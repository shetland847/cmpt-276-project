
Iteration One
==============

<p>
July 2nd, 2019, Version 0.1

Scott Hetland shetland@sfu.ca

Ryan Lintott  rlintott@sfu.ca

David Liu     dla68@sfu.ca

Link: https://gitlab.com/shetland847/cmpt-276-project
</p>

-----------------
SFU CS Grad Buddy
-----------------

<p>
Overview

This web application will help SFU Computer Science undergraduate students plan their course loads. The target audience are SFU undergraduate students in Computing Science, Although the app could expand in the future to include all programs, and possible other universities. Currently there is no easy way to see what courses a student needs and what courses will be available in the coming semesters, except by searching for rather difficult to access pages on SFU’s labyrinth of websites. This application will package data about credits and courses taken and needed into a single easy to use interface so that students can easily plan their university career. It will provide information about courses that will be available in future semesters, and course reviews which other users will provide. As it may be difficult to foresee how the different courses will conflict when it comes time to actually enroll, the app will show the user every course they could possibly take that semester, so that they have a large range of options. It will also calculate information about credits needed. It will use SFU’s course outline and academic calendar APIs. Some information may have to be manually inserted into the app database and users may have to manually enter some information about their current status.

Features:
* Automatically displays all courses in your major that you could take in the upcoming year, in an intuitive calendar visualization.
* Clicking on courses opens a new page or popup which shows information about the course, tentative instructor if known, and reviews of the course that others users may submit. 
* A sidebar will display your graduation progress. It will show the number of credits currently taken and currently needed, included upper and lower division and major credits. 
* Users can highlight courses to simulating taking the course. This may update the courses available for them and will increase their graduation progress to take so that they can test different course loads to see what the best choice is.
</p>